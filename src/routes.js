/*!

=========================================================
* Paper Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

* Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import Dashboard from "views/Dashboard.js";
import Notifications from "views/Notifications.js";
import Icons from "views/Icons.js";
import Typography from "views/Typography.js";
import TableList from "views/Tables.js";
import Maps from "views/Map.js";
import addUser from "views/userManagement/addUser";
import contractor from "views/contractor/contractor";
import productMaterial from "views/inventory/productMaterial";
import Inquiry from "views/Inquiry";
import CostEstimation from "views/CostEstimation";
import material from "views/inventory/material";
import product from "views/inventory/product";
import addCustomer from "views/userManagement/addCustomer";
import UserPage from "views/User.js";
import UpgradeToPro from "views/Upgrade.js";

var routes = [
  {
    path: "/addUser",
    name: "addUser ",
    icon: "nc-icon nc-single-02",
    component: addUser,
    layout: "/admin",
  },
  {
    path: "/addCustomer",
    name: "addCustomer ",
    icon: "nc-icon nc-single-02",
    component: addCustomer,
    layout: "/admin",
  },
  {
    path: "/material",
    name: "material ",
    icon: "nc-icon nc-single-02",
    component: material,
    layout: "/admin",
  },
  {
    path: "/product",
    name: "product ",
    icon: "nc-icon nc-single-02",
    component: product,
    layout: "/admin",
  },
  {
    path: "/productMaterial",
    name: "productMaterial ",
    icon: "nc-icon nc-single-02",
    component: productMaterial,
    layout: "/admin",
  },
  {
    path: "/contractor",
    name: "contractor ",
    icon: "nc-icon nc-single-02",
    component: contractor,
    layout: "/admin",
  },
  {
    path: "/inquiry",
    name: "Inquiry ",
    icon: "nc-icon nc-single-02",
    component: Inquiry,
    layout: "/admin",
  },
  {
    path: "/costEstimation",
    name: "CostEstimation ",
    icon: "nc-icon nc-single-02",
    component: CostEstimation,
    layout: "/admin",
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin",
  },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   icon: "nc-icon nc-diamond",
  //   component: Icons,
  //   layout: "/admin",
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   icon: "nc-icon nc-pin-3",
  //   component: Maps,
  //   layout: "/admin",
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   icon: "nc-icon nc-bell-55",
  //   component: Notifications,
  //   layout: "/admin",
  // },
  {
    path: "/user-page",
    name: "User Profile",
    icon: "nc-icon nc-single-02",
    component: UserPage,
    layout: "/admin",
  },

  // {
  //   path: "/tables",
  //   name: "Table List",
  //   icon: "nc-icon nc-tile-56",
  //   component: TableList,
  //   layout: "/admin",
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   icon: "nc-icon nc-caps-small",
  //   component: Typography,
  //   layout: "/admin",
  // },
  // {
  //   pro: true,
  //   path: "/upgrade",
  //   name: "Upgrade to PRO",
  //   icon: "nc-icon nc-spaceship",
  //   component: UpgradeToPro,
  //   layout: "/admin",
  // },
];
export default routes;
