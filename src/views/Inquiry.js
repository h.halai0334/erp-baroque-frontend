import React, { useState, useEffect } from "react";
import "./userManagement/App.css";
import { forwardRef } from "react";
import Avatar from "react-avatar";
import Grid from "@material-ui/core/Grid";
import AddProduct from "../components/Modals/AddProduct";
import CreateInquiryModal from "../components/Modals/CreateInquiryModal";

import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  FormGroup,
  Form,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";
//import Button from "@material-ui/core/Button";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import MaterialTable from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import axios from "axios";
import Alert from "@material-ui/lab/Alert";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const filter = createFilterOptions();
toast.configure();
var code = "";
var inquiryCode = "";
var customerCode = "";
const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const api = axios.create({
  baseURL: `https://reqres.in/api`,
});

function validateEmail(email) {
  const re = /^((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))$/;
  return re.test(String(email).toLowerCase());
}
function getUsers(setData, query = { IsDescending: true, Page: 1 }) {
  const token = localStorage.getItem("login");
  let baseURL =
    "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User?";
  var searchParams = new URLSearchParams(query);

  for (let [key, value] of Object.entries(query)) {
    console.log("key, value: ", key, value);
    searchParams.append(key, value);
  }

  let URl = baseURL + searchParams.toString();

  fetch(URl, {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((res) => {
      console.log("res", res);
      setData(res.data);
    });
}
function App() {
  // old colum
  var columns = [
    { title: "id", field: "id", hidden: true },

    {
      title: "Name",
      field: "name",
    },
    { title: "Description", field: "productDescription" },
    { title: "Quantity", field: "productQunatity" },
  ];

  let [tableData, setTableData] = useState([]); //table data

  //for error handling
  const [iserror, setIserror] = useState(false);
  const [errorMessages, setErrorMessages] = useState([]);
  const [productList, setProductList] = useState([]);
  const [customerList, setCustomerList] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [openInquiry, setOpenInquiry] = React.useState(false);
  const [openProduct, setOpenProduct] = React.useState(false);

  const [productNameData, setProductName] = useState("");

  const [eachproductDescripData, setEachProductDescrip] = useState("");
  const [eachproductQuantityData, setEachProductQuantity] = useState("");

  const [openProdcutForInquiry, setProdcutForInquiry] = React.useState(false);

  const [customersDetails, setCustomerDetails] = React.useState({});
  let [tableDataForEachRow, setTableDataForEachRow] = React.useState([]);

  const [customernameData, setcustomerNameData] = useState("");
  const [customerEmailData, setcustomerEmailData] = useState("");
  const [customerMobileNumData, setcustomerMobileNumData] = useState();
  const [customerPhoneNumData, setcustomerPhoneNumData] = useState();
  const [customerAddessData, setcustomerAddessData] = useState();
  useEffect(() => {
    // getUsers(setData);
    getProductsList(setProductList);
    // getCustomerList(setCustomerList);
  }, []);
  function getProductCode() {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Product/code",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        code = res.code;
      });
  }
  function getCustomerCode() {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Customer/code",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        customerCode = res.code;
      });
  }
  function getInquiryCode() {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Inquiry/code",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        inquiryCode = res.code;
      });
  }
  function getProductsList(setProductList) {
    const token = localStorage.getItem("login");

    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Product/dropDown?Page=1",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        const productList = res.data.map((d) => {
          return {
            id: d.id,
            title: d.name,
          };
        });
        setProductList(productList);
      });
  }

  const addInquiry = (e) => {
    getInquiryCode();
    e.preventDefault();
    // getCustomerCode();
    if (tableData.length === 0) {
      toast("Add Atleast 1 Prodcut");
    } else {
      setOpenInquiry(true);
    }
  };
  const addInquiryProduct = (e) => {
    setProdcutForInquiry(true);
  };
  const addCustomer = (e) => {
    // e.preventDefault();
    getCustomerCode();
    setOpen(true);
  };
  const addProduct = (e) => {
    //e.preventDefault();

    setOpenProduct(true);
    getProductCode();
  };
  const handleClose = () => {
    setProdcutForInquiry(false);
    //setOpenProduct(false);
  };
  const handleCloseInquiryModal = () => {
    setOpenInquiry(false);
    //setOpenProduct(false);
  };
  const handleCloseProduct = () => {
    setOpenInquiry(false);
    //setOpenProduct(false);
  };

  // const addproductData = () => {
  //   let errorList = [];

  //   if (productNameData === undefined) {
  //     errorList.push("Please enter first name");
  //   }
  //   // if (productDescripData === undefined) {
  //   //   errorList.push("Please enter a valid Description");
  //   // }
  //   if (productSaleData === undefined) {
  //     errorList.push("Please enter a valid Sale");
  //   }

  //   if (errorList.length < 1) {
  //     console.log(code);
  //     //no error
  //     const token = localStorage.getItem("login");
  //     fetch(
  //       "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Product",
  //       {
  //         method: "POST",
  //         headers: {
  //           Authorization: "Bearer " + token,
  //           "Content-Type": "application/json",
  //         },
  //         body: JSON.stringify({
  //           code: code,
  //           name: productNameData,
  //           description: "", // productDescripData
  //           image: "",
  //           sale: +productSaleData,
  //         }),
  //       }
  //     )
  //       .then((response) => response.json())
  //       .then((res) => {
  //         let data = {
  //           id: res.id,
  //           name: res.name,
  //         };
  //         productList.push(data);
  //         setOpenProduct(false);
  //       });
  //   } else {
  //     setErrorMessages(errorList);
  //     setIserror(true);
  //   }
  // };
  const addCustomerData = () => {
    // let errorList = [];
    // // if (productNameData === undefined) {
    // //   errorList.push("Please enter first name");
    // // }
    // // if (productDescripData === undefined) {
    // //   errorList.push("Please enter a valid Description");
    // // }
    // // if (productSaleData === undefined) {
    // //   errorList.push("Please enter a valid Sale");
    // // }
    // if (errorList.length < 1) {
    //   console.log(code);
    //   //no error
    //   const token = localStorage.getItem("login");
    //   fetch(
    //     "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Customer",
    //     {
    //       method: "POST",
    //       headers: {
    //         Authorization: "Bearer " + token,
    //         "Content-Type": "application/json",
    //       },
    //       body: JSON.stringify({
    //         code: customerCode,
    //         name: customernameData,
    //         address: customerAddessData,
    //         phoneNumber: customerPhoneNumData,
    //         mobileNumber: customerMobileNumData,
    //         email: customerEmailData,
    //         image: "",
    //       }),
    //     }
    //   )
    //     .then((response) => response.json())
    //     .then((res) => {
    //       let data = {
    //         address: res.address,
    //         code: res.code,
    //         email: res.email,
    //         image: res.image,
    //         mobileNumber: res.mobileNumber,
    //         name: res.name,
    //         phoneNumber: res.phoneNumber,
    //       };
    //       customerList.push(data);
    //       handleClose();
    //     });
    // } else {
    //   setErrorMessages(errorList);
    //   setIserror(true);
    // }
  };

  const productname = (event) => {
    setProductName(event.target.value);
  };
  const productDescrip = (event) => {
    // setProductDescrip(event.target.value);
  };

  const customername = (event) => {
    setcustomerNameData(event.target.value);
  };
  const customerEmail = (event) => {
    setcustomerEmailData(event.target.value);
  };
  const customerPhoneNum = (event) => {
    setcustomerPhoneNumData(event.target.value);
  };
  const customerMobileNum = (event) => {
    setcustomerMobileNumData(event.target.value);
  };
  const customerAddress = (event) => {
    setcustomerAddessData(event.target.value);
  };
  // const handleRowUpdate = (newData, oldData, resolve) => {
  //   //validation
  //   let errorList = [];
  //   if (newData.first_name === "") {
  //     errorList.push("Please enter first name");
  //   }
  //   if (newData.last_name === "") {
  //     errorList.push("Please enter last name");
  //   }
  //   if (newData.email === "" || validateEmail(newData.email) === false) {
  //     errorList.push("Please enter a valid email");
  //   }

  //   if (errorList.length < 1) {
  //     console.log("new data", newData);
  //     //no error
  //     const token = localStorage.getItem("login");
  //     fetch(
  //       "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User/" +
  //         newData.id,
  //       {
  //         method: "PUT",
  //         headers: {
  //           Authorization: "Bearer " + token,
  //           "Content-Type": "application/json",
  //         },
  //         body: JSON.stringify({
  //           id: newData.id,
  //           email: newData.email,
  //           name: newData.name,
  //           image: "",
  //           role: newData.role,
  //           address: newData.address,
  //           createdDate: newData.createdDate,
  //           phoneNumber: newData.phoneNumber,
  //           isEnabled: true,
  //           UserId: newData.id,
  //         }),
  //       }
  //     )
  //       .then((response) => response.json())
  //       .then((res) => {
  //         const dataUpdate = [...data];
  //         const index = oldData.tableData.id;
  //         dataUpdate[index] = newData;
  //         setData([...dataUpdate]);
  //         resolve();
  //         setIserror(false);
  //         setErrorMessages([]);
  //       });
  //     //   api
  //     //     .get("/users")
  //     //     .then((res) => {
  //     //       console.log("res", res);
  //     //       setData(res.data.data);
  //     //     })
  //     //     .catch((error) => {
  //     //       console.log("Error");
  //     //     });
  //     //
  //   } else {
  //     setErrorMessages(errorList);
  //     setIserror(true);
  //     resolve();
  //   }
  // };

  // const handleRowAdd = (newData, resolve) => {
  //   //validation
  //   let errorList = [];

  //   if (newData.name === undefined) {
  //     errorList.push("Please enter first name");
  //   }
  //   if (newData.phoneNumber === undefined) {
  //     errorList.push("Please enter a valid Phone Number");
  //   }
  //   if (newData.email === undefined || validateEmail(newData.email) === false) {
  //     errorList.push("Please enter a valid email");
  //   }

  //   if (errorList.length < 1) {
  //     console.log("new data", newData);
  //     //no error
  //     const token = localStorage.getItem("login");
  //     fetch(
  //       "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User",
  //       {
  //         method: "POST",
  //         headers: {
  //           Authorization: "Bearer " + token,
  //           "Content-Type": "application/json",
  //         },
  //         body: JSON.stringify({
  //           email: newData.email,
  //           name: newData.name,
  //           image: "",
  //           role: newData.role,
  //           phoneNumber: newData.phoneNumber,
  //           address: newData.address,
  //           password: newData.password,
  //         }),
  //       }
  //     )
  //       .then((response) => response.json())
  //       .then((res) => {
  //         let dataToAdd = [...data];
  //         dataToAdd.push(newData);
  //         setData(dataToAdd);
  //         resolve();
  //         setErrorMessages([]);
  //         setIserror(false);
  //       });
  //     //   api
  //     //     .get("/users")
  //     //     .then((res) => {
  //     //       console.log("res", res);
  //     //       setData(res.data.data);
  //     //     })
  //     //     .catch((error) => {
  //     //       console.log("Error");
  //     //     });
  //     //
  //   } else {
  //     setErrorMessages(errorList);
  //     setIserror(true);
  //     resolve();
  //   }
  // };

  // const handleRowDelete = (oldData, resolve) => {
  //   const token = localStorage.getItem("login");
  //   fetch(
  //     "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User/" +
  //       oldData.id,
  //     {
  //       method: "DELETE",
  //       headers: {
  //         Authorization: "Bearer " + token,
  //         "Content-Type": "application/json",
  //       },
  //     }
  //   )
  //     .then((response) => response.json())
  //     .then((res) => {
  //       const dataDelete = [...data];
  //       const index = oldData.tableData.id;
  //       dataDelete.splice(index, 1);
  //       setData([...dataDelete]);
  //       resolve();
  //     })
  //     .catch((error) => {
  //       setErrorMessages(["Delete failed! Server error"]);
  //       setIserror(true);
  //       resolve();
  //     });
  //   // api
  //   //   .delete("/users/" + oldData.id)
  //   //   .then((res) => {
  //   //     const dataDelete = [...data];
  //   //     const index = oldData.tableData.id;
  //   //     dataDelete.splice(index, 1);
  //   //     setData([...dataDelete]);
  //   //     resolve();
  //   //   })
  //   //   .catch((error) => {
  //   //     setErrorMessages(["Delete failed! Server error"]);
  //   //     setIserror(true);
  //   //     resolve();
  //   //   });
  // };
  // function onSearchChange(value) {
  //   let query = { IsDescending: true, Page: 1 };
  //   query.search = value;

  //   getUsers(setData, query);
  // }
  // function onSelectProdct(e, value) {
  //   const completeTableData = {
  //     items: [
  //       {
  //         productId: tableDataForEachRow[0].id,
  //         description: eachproductDescripData,
  //         quantity: +eachproductQuantityData,
  //       },
  //     ],
  //   };

  //   // tableData = [...tableData, completeTableData];
  //   setTableData(tableData);
  // }
  function onSelectEachProduct(e, value) {
    if (value.id) {
      const token = localStorage.getItem("login");
      fetch(
        `http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Product/detail/${value.id}`,
        {
          method: "GET",
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((res) => {
          const productTableData = {
            id: res.id,
            name: res.name,
            image: res.image,
            code: "P_2",
          };
          const TableDataForEachRowIds = tableDataForEachRow.map((td) => td.id);
          const isDataInclude = TableDataForEachRowIds.includes(
            productTableData.id
          );
          if (isDataInclude) {
            // toast yes include do not need to add in tableData
          } else {
            tableDataForEachRow = [productTableData];
          }

          setTableDataForEachRow(tableDataForEachRow);
        });
    } else {
      addProduct();
    }
  }

  function getAddProduct(addProductData) {
    tableData = [...tableData, addProductData];
    setTableData(tableData);
  }

  return (
    <div className="App">
      <Row>
        <Col md="4"></Col>
        <Col md="12">
          <Card className="card-user">
            <CardHeader>
              <CardTitle tag="h5">Create Inquiry</CardTitle>
            </CardHeader>
            <CardBody>
              <Form style={{ height: 100 }}>
                <Row>
                  <Col className="pr-1" md="4">
                    <FormGroup></FormGroup>
                  </Col>
                  <Col className="pr-1" md="4">
                    <FormGroup></FormGroup>
                  </Col>

                  <Col className="pr-2" md="5"></Col>
                </Row>

                <Row>
                  <div className="update ml-auto mr-auto">
                    <Button
                      className="btn-round"
                      color="primary"
                      type="submit"
                      onClick={addInquiry}
                    >
                      Create Inquiry
                    </Button>
                  </div>
                </Row>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <div className="update ml-auto mr-auto">
        <Button
          className="btn-round"
          color="primary"
          type="submit"
          onClick={addInquiryProduct}
        >
          Add Product
        </Button>
      </div>
      <Grid container spacing={2}>
        <Grid item xs={12}></Grid>
        <Grid item xs={12}>
          <div>
            {iserror && (
              <Alert severity="error">
                {errorMessages.map((msg, i) => {
                  return <div key={i}>{msg}</div>;
                })}
              </Alert>
            )}
          </div>

          <MaterialTable
            title="PRODUCT"
            columns={columns}
            data={tableData}
            icons={tableIcons}
          />
        </Grid>
        <Grid item xs={3}></Grid>
      </Grid>

      {openInquiry ? (
        <CreateInquiryModal
          tableData={tableData}
          openInquiry={openInquiry}
          handleCloseInquiryModal={handleCloseInquiryModal}
          addCustomer={addCustomer}
          // handleClose={handleClose}
        />
      ) : null}

      {openProdcutForInquiry ? (
        <AddProduct
          productList={productList}
          handleClose={handleClose}
          openProdcutForInquiry={openProdcutForInquiry}
          setProdcutForInquiry={setProdcutForInquiry}
          getAddProduct={getAddProduct}
        />
      ) : null}
    </div>
  );
}

export default App;
