/*!

=========================================================
* Paper Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

* Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";



class User extends React.Component {
  constructor(props){
    super(props);
    this.state={
      fullName:'',
      email:'',
      image:"",
      phoneNo:'',
      address:"",
      id:'',
      role:'',
      createdDate:''
    }
  }

  componentDidMount(){
    this.userDetails()
  }

  userDetails(){
    // var token = localStorage.getItem('id_token');
    // var decodedData = jwt_decode(token);
    // console.log('data',decodedData.image)
    // this.setState({
    //   fullName:decodedData.fullName,
    //   email:decodedData.email,
    //   image:decodedData.image,
    //   phoneNo:decodedData.phoneNumber

    // })
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User/profile",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
          Accept: '*/*',
          AcceptEncoding: 'gizp,deflate,br'
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        console.log('data',data)
        this.setState({
          fullName:data.name,
          email:data.email,
          image:data.image,
          phoneNo:data.phoneNumber,
          address:data.address,
          id:data.id,
          role:data.role,
          createdDate:data.createdDate,
        })
      });
   
  }
  onNameChange(name){
    this.setState({
         fullName: name
    });
}
onEmailChange(email){
  this.setState({
       email: email
  });
}
onPhoneChange(phone){
  this.setState({
       phoneNo: phone
  });
}
onAddressChange(address){
  this.setState({
       address: address
  });
}

updateUser(e){
  e.preventDefault();
  const token = localStorage.getItem("login");
  fetch(
    "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User/profile",
    {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + token,
        Accept: '*/*',
        AcceptEncoding: 'gizp,deflate,br',
        "Content-Type":"application/json"
      },
      body:JSON.stringify({
        id: this.state.id,
        email: this.state.email,
        name: this.state.fullName,
        image: this.state.image,
        role: this.state.role,
        phoneNumber: this.state.phoneNo,
        address: this.state.address,
        createdDate: this.state.createdDate,
        isEnabled: true
      })
    }
  )
    .then((response) => response.json())
    .then((data) => {
      if(data.isEnabled){
        alert('Profile Has Been Updated')
      }
      else{
        alert('Something Wrong')
      }
    });
}
  render() {
    console.log('Name',this.state.phoneNo)
    return (
      <>
        <div className="content">
          <Row>
            <Col md="4">
              <Card className="card-user">
                <div className="image">
                  <img
                    alt="..."
                    src={require("assets/img/damir-bosnjak.jpg")}
                  />
                </div>
                <CardBody>
                  <div className="author">
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <img
                        alt="..."
                        className="avatar border-gray"
                        src={require("assets/img/mike.jpg")}
                      />
                      <h5 className="title">{this.state.fullName}</h5>
                    </a>
                    <p className="description">@chetfaker</p>
                  </div>
                  <p className="description text-center">
                    "I like the way you work it <br />
                    No diggity <br />I wanna bag it up"
                  </p>
                </CardBody> 
                <CardFooter>
                  <hr />
                  <div className="button-container">
                    <Row>
                      <Col className="ml-auto" lg="3" md="6" xs="6">
                        <h5>
                          12 <br />
                          <small>Files</small>
                        </h5>
                      </Col>
                      <Col className="ml-auto mr-auto" lg="4" md="6" xs="6">
                        <h5>
                          2GB <br />
                          <small>Used</small>
                        </h5>
                      </Col>
                      <Col className="mr-auto" lg="3">
                        <h5>
                          24,6$ <br />
                          <small>Spent</small>
                        </h5>
                      </Col>
                    </Row>
                  </div>
                </CardFooter>
              </Card>
              {/* <Card>
                <CardHeader>
                  <CardTitle tag="h4">Team Members</CardTitle>
                </CardHeader>
                <CardBody>
                  <ul className="list-unstyled team-members">
                    <li>
                      <Row>
                        <Col md="2" xs="2">
                          <div className="avatar">
                            <img
                              alt="..."
                              className="img-circle img-no-padding img-responsive"
                              src={require("assets/img/faces/ayo-ogunseinde-2.jpg")}
                            />
                          </div>
                        </Col>
                        <Col md="7" xs="7">
                          DJ Khaled <br />
                          <span className="text-muted">
                            <small>Offline</small>
                          </span>
                        </Col>
                        <Col className="text-right" md="3" xs="3">
                          <Button
                            className="btn-round btn-icon"
                            color="success"
                            outline
                            size="sm"
                          >
                            <i className="fa fa-envelope" />
                          </Button>
                        </Col>
                      </Row>
                    </li>
                    <li>
                      <Row>
                        <Col md="2" xs="2">
                          <div className="avatar">
                            <img
                              alt="..."
                              className="img-circle img-no-padding img-responsive"
                              src={require("assets/img/faces/joe-gardner-2.jpg")}
                            />
                          </div>
                        </Col>
                        <Col md="7" xs="7">
                          Creative Tim <br />
                          <span className="text-success">
                            <small>Available</small>
                          </span>
                        </Col>
                        <Col className="text-right" md="3" xs="3">
                          <Button
                            className="btn-round btn-icon"
                            color="success"
                            outline
                            size="sm"
                          >
                            <i className="fa fa-envelope" />
                          </Button>
                        </Col>
                      </Row>
                    </li>
                    <li>
                      <Row>
                        <Col md="2" xs="2">
                          <div className="avatar">
                            <img
                              alt="..."
                              className="img-circle img-no-padding img-responsive"
                              src={require("assets/img/faces/clem-onojeghuo-2.jpg")}
                            />
                          </div>
                        </Col>
                        <Col className="col-ms-7" xs="7">
                          Flume <br />
                          <span className="text-danger">
                            <small>Busy</small>
                          </span>
                        </Col>
                        <Col className="text-right" md="3" xs="3">
                          <Button
                            className="btn-round btn-icon"
                            color="success"
                            outline
                            size="sm"
                          >
                            <i className="fa fa-envelope" />
                          </Button>
                        </Col>
                      </Row>
                    </li>
                  </ul>
                </CardBody>
              </Card> */}
            </Col>
            <Col md="8">
              <Card className="card-user">
                <CardHeader>
                  <CardTitle tag="h5">Edit Profile</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col className="pr-1" md="5">
                        <FormGroup>
                          <label>Name</label>
                          <Input
                           onChange={e => this.onNameChange(e.target.value)}
                            defaultValue={this.state.fullName}
                          
                            placeholder="Name"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="3">
                        <FormGroup>
                          <label>Email</label>
                          <Input
                            onChange={e => this.onEmailChange(e.target.value)}
                            defaultValue={this.state.email}
                            placeholder="Email"
                            type="email"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-1" md="4">
                        <FormGroup>
                          <label htmlFor="exampleInputEmail1">
                           Phone
                          </label>
                          <Input  
                          onChange={e => this.onPhoneChange(e.target.value)}
                          defaultValue={this.state.phoneNo} 
                          placeholder="Phone" type="phone " />
                        </FormGroup>
                      </Col>
                    </Row>
                      {/* <Row>
                        <Col className="pr-1" md="6">
                          <FormGroup>
                            <label>First Name</label>
                            <Input
                              defaultValue="Chet"
                              placeholder="Company"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col className="pl-1" md="6">
                          <FormGroup>
                            <label>Last Name</label>
                            <Input
                              defaultValue="Faker"
                              placeholder="Last Name"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                      </Row> */}
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Address</label>
                          <Input
                           onChange={e => this.onAddressChange(e.target.value)}
                            defaultValue={this.state.address}
                            placeholder="Address"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    {/* <Row>
                      <Col className="pr-1" md="4">
                        <FormGroup>
                          <label>City</label>
                          <Input
                            defaultValue="Melbourne"
                            placeholder="City"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="4">
                        <FormGroup>
                          <label>Country</label>
                          <Input
                            defaultValue="Australia"
                            placeholder="Country"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-1" md="4">
                        <FormGroup>
                          <label>Postal Code</label>
                          <Input placeholder="ZIP Code" type="number" />
                        </FormGroup>
                      </Col>
                    </Row> */}
                    {/* <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>About Me</label>
                          <Input
                            type="textarea"
                            defaultValue="Oh so, your weak rhyme You doubt I'll bother, reading into it"
                          />
                        </FormGroup>
                      </Col>
                    </Row> */}
                    <Row>
                      <div className="update ml-auto mr-auto">
                        <Button
                          className="btn-round"
                          color="primary"
                          type="submit"
                          onClick={(e) => this.updateUser(e) }
                        >
                          Update Profile
                        </Button>
                      </div>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default User;
