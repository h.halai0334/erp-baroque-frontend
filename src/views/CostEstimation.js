import React, { useState, useEffect } from "react";
import "../views/userManagement/App.css";
import { forwardRef } from "react";
import Avatar from "react-avatar";
import Grid from "@material-ui/core/Grid";
import { Button } from "reactstrap";
import MaterialTable from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import axios from "axios";
import Alert from "@material-ui/lab/Alert";
import * as constants from "../components/constants/constant";
import CreateCostEstimationModal from "../components/Modals/CreateCostEstimationModal";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const api = axios.create({
  baseURL: `https://reqres.in/api`,
});

function validateEmail(email) {
  const re = /^((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))$/;
  return re.test(String(email).toLowerCase());
}

function CostEstimation() {
  var columns = [
    { title: "Inquiry Code", field: "code" },
    // {
    //   title: "Avatar",
    //   render: (rowData) => (
    //     <Avatar
    //       maxInitials={1}
    //       size={40}
    //       round={true}
    //       name={rowData === undefined ? " " : rowData.first_name}
    //     />
    //   ),
    // },
    { title: "Customer Name", field: "customer" },
    { title: "Created By", field: "creator" },
    { title: "Status    ", field: "status" },
  ];
  let [data, setData] = useState([]); //table data

  //for error handling
  const [iserror, setIserror] = useState(false);
  const [errorMessages, setErrorMessages] = useState([]);
  const [
    openCreateCostEstimationModal,
    setCreateCostEstimationModal,
  ] = useState(false);
  let [selectedRowData, setSelectedRowData] = useState({});

  useEffect(() => {
    getUsers();
    //   api
    //     .get("/users")
    //     .then((res) => {
    //       console.log("res", res);
    //       setData(res.data.data);
    //     })
    //     .catch((error) => {
    //       console.log("Error");
    //     });
    //
  }, []);

  const handleRowUpdate = (newData, oldData, resolve) => {
    //validation
    let errorList = [];
    if (newData.first_name === "") {
      errorList.push("Please enter first name");
    }
    if (newData.last_name === "") {
      errorList.push("Please enter last name");
    }
    if (newData.email === "" || validateEmail(newData.email) === false) {
      errorList.push("Please enter a valid email");
    }

    if (errorList.length < 1) {
      console.log("new data", newData);
      //no error
      const token = localStorage.getItem("login");
      fetch(
        "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User/" +
          newData.id,
        {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: newData.id,
            email: newData.email,
            name: newData.name,
            image: "",
            role: newData.role,
            address: newData.address,
            createdDate: newData.createdDate,
            phoneNumber: newData.phoneNumber,
            isEnabled: true,
            UserId: newData.id,
          }),
        }
      )
        .then((response) => response.json())
        .then((res) => {
          const dataUpdate = [...data];
          const index = oldData.tableData.id;
          dataUpdate[index] = newData;
          setData([...dataUpdate]);
          resolve();
          setIserror(false);
          setErrorMessages([]);
        });
      //   api
      //     .get("/users")
      //     .then((res) => {
      //       console.log("res", res);
      //       setData(res.data.data);
      //     })
      //     .catch((error) => {
      //       console.log("Error");
      //     });
      //
    } else {
      setErrorMessages(errorList);
      setIserror(true);
      resolve();
    }
  };

  const handleRowAdd = (newData, resolve) => {
    //validation
    let errorList = [];

    if (newData.name === undefined) {
      errorList.push("Please enter first name");
    }
    if (newData.phoneNumber === undefined) {
      errorList.push("Please enter a valid Phone Number");
    }
    if (newData.email === undefined || validateEmail(newData.email) === false) {
      errorList.push("Please enter a valid email");
    }

    if (errorList.length < 1) {
      console.log("new data", newData);
      //no error
      const token = localStorage.getItem("login");
      fetch(
        "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User",
        {
          method: "POST",
          headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: newData.email,
            name: newData.name,
            image: "",
            role: newData.role,
            phoneNumber: newData.phoneNumber,
            address: newData.address,
            password: newData.password,
          }),
        }
      )
        .then((response) => response.json())
        .then((res) => {
          let dataToAdd = [...data];
          dataToAdd.push(newData);
          setData(dataToAdd);
          resolve();
          setErrorMessages([]);
          setIserror(false);
        });
      //   api
      //     .get("/users")
      //     .then((res) => {
      //       console.log("res", res);
      //       setData(res.data.data);
      //     })
      //     .catch((error) => {
      //       console.log("Error");
      //     });
      //
    } else {
      setErrorMessages(errorList);
      setIserror(true);
      resolve();
    }
  };

  const handleRowDelete = (oldData, resolve) => {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/User/" +
        oldData.id,
      {
        method: "DELETE",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        const dataDelete = [...data];
        const index = oldData.tableData.id;
        dataDelete.splice(index, 1);
        setData([...dataDelete]);
        resolve();
      })
      .catch((error) => {
        setErrorMessages(["Delete failed! Server error"]);
        setIserror(true);
        resolve();
      });
    // api
    //   .delete("/users/" + oldData.id)
    //   .then((res) => {
    //     const dataDelete = [...data];
    //     const index = oldData.tableData.id;
    //     dataDelete.splice(index, 1);
    //     setData([...dataDelete]);
    //     resolve();
    //   })
    //   .catch((error) => {
    //     setErrorMessages(["Delete failed! Server error"]);
    //     setIserror(true);
    //     resolve();
    //   });
  };
  function onSearchChange(value) {
    let query = { IsDescending: true, Page: 1 };
    query.search = value;

    getUsers(setData, query);
  }
  function getUsers(query = { IsDescending: true, Page: 1 }) {
    const token = localStorage.getItem("login");
    const userId = localStorage.getItem("userId");
    let baseURL = `http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Inquiry?UserId=${userId}&Descending=true&Page=1`;
    var searchParams = new URLSearchParams(query);

    for (let [key, value] of Object.entries(query)) {
      console.log("key, value: ", key, value);
      searchParams.append(key, value);
    }

    let URl = baseURL;
    //+ searchParams.toString();

    fetch(URl, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        let tableData = [];
        tableData = [
          ...tableData,
          ...res.data.map((d) => {
            return {
              id: d.id,
              code: d.code,
              creator: d.creator,
              customer: d.customer,
              status: constants.inquiryStatusMapping[d.status],
            };
          }),
        ];
        setData(tableData);
      });
  }
  const CostEstimation = (rowId) => {
    console.log(rowId);
  };

  const openCostEstimationModal = (rowData) => {
    const id = rowData.id;

    const token = localStorage.getItem("login");
    fetch(
      `http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Inquiry/detail/${id}`,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        selectedRowData = res;
        setSelectedRowData(selectedRowData);
        setCreateCostEstimationModal(true);
      });
  };
  const handlecloseModal = () => {
    setCreateCostEstimationModal(false);
  };

  return (
    <div className="App">
      <Grid container spacing={2}>
        <Grid item xs={12}></Grid>
        <Grid item xs={12}>
          <div>
            {iserror && (
              <Alert severity="error">
                {errorMessages.map((msg, i) => {
                  return <div key={i}>{msg}</div>;
                })}
              </Alert>
            )}
          </div>

          <MaterialTable
            title="Pending Iquiries"
            columns={columns}
            data={data}
            icons={tableIcons}
            actions={[
              {
                icon: "add",
                tooltip: "Add User",
                label: "talha",
                isFreeAction: false,
                onClick: (event, rowData) => {},

                // icon: "A", // <--- This will be overridden by the components prop Actions
                // tooltip: "Cost Estimation1",
                // isFreeAction: true,
                // onClick: (event, rowData) => {
                //   debugger;
                //   console.log("You are editing " + rowData.fname);
                // },
              },
            ]}
            components={{
              Action: (props) => (
                <Button
                  className="btn-round"
                  color="primary"
                  onClick={(event, rowData) => {
                    openCostEstimationModal(props.data);
                  }}
                >
                  Cost Estimation
                </Button>
              ),
            }}
            // onSearchChange={onSearchChange}
            // editable={{
            //   onRowUpdate: (newData, oldData) =>
            //     new Promise((resolve) => {
            //       handleRowUpdate(newData, oldData, resolve);
            //     }),
            //   onRowAdd: (newData) =>
            //     new Promise((resolve) => {
            //       handleRowAdd(newData, resolve);
            //     }),
            //   onRowDelete: (oldData) =>
            //     new Promise((resolve) => {
            //       handleRowDelete(oldData, resolve);
            //     }),
            // }}
          />
        </Grid>
        <Grid item xs={3}></Grid>
      </Grid>
      {openCreateCostEstimationModal ? (
        <CreateCostEstimationModal
          selectedRowData={selectedRowData}
          openCreateCostEstimationModal={openCreateCostEstimationModal}
          handlecloseModal={handlecloseModal}
        />
      ) : null}
    </div>
  );
}

export default CostEstimation;
