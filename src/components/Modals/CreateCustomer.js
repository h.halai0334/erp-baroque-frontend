import React, { useState, useEffect } from "react";
// import "./userManagement/App.css";
import { forwardRef } from "react";
import Avatar from "react-avatar";
import Grid from "@material-ui/core/Grid";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  FormGroup,
  Form,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";
//import Button from "@material-ui/core/Button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
var customerCode = "";
toast.configure();
function CreateCustomerModal(props) {
  useEffect(() => {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Customer/code",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        customerCode = res.code;
      });
  }, []);
  const [customernameData, setcustomerNameData] = useState("");
  const [customerEmailData, setcustomerEmailData] = useState("");
  const [customerMobileNumData, setcustomerMobileNumData] = useState();
  const [customerPhoneNumData, setcustomerPhoneNumData] = useState();
  const [customerAddessData, setcustomerAddessData] = useState();
  const customername = (event) => {
    setcustomerNameData(event.target.value);
  };
  const customerEmail = (event) => {
    setcustomerEmailData(event.target.value);
  };
  const customerPhoneNum = (event) => {
    setcustomerPhoneNumData(event.target.value);
  };
  const customerMobileNum = (event) => {
    setcustomerMobileNumData(event.target.value);
  };
  const customerAddress = (event) => {
    setcustomerAddessData(event.target.value);
  };

  //   function handleCloseProductModal() {
  //     setOpenProductModal(true);
  //   }
  function createCustomer() {
    let errorList = [];

    // if (productNameData === undefined) {
    //   errorList.push("Please enter first name");
    // }
    // if (productDescripData === undefined) {
    //   errorList.push("Please enter a valid Description");
    // }
    // if (productSaleData === undefined) {
    //   errorList.push("Please enter a valid Sale");
    // }

    if (errorList.length < 1) {
      //console.log(code);
      //no error
      const token = localStorage.getItem("login");
      fetch(
        "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Customer",
        {
          method: "POST",
          headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            code: customerCode,
            name: customernameData,
            address: customerAddessData,
            phoneNumber: customerPhoneNumData,
            mobileNumber: customerMobileNumData,
            email: customerEmailData,
            image: "",
          }),
        }
      )
        .then((response) => response.json())
        .then((res) => {
          if (res.id) {
            toast("Customer Has Bee Created Successfully");
            handleCloseCreateCustomer();
          } else {
            toast("Found Some Error");
          }
        });
    }
  }
  function handleCloseCreateCustomer() {
    props.handleCloseCreateCustomer();
  }
  return (
    <div className="App">
      <Dialog
        open={props.openCreate}
        onClose={props.handleCloseCreateCustomer}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create Customer</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            onChange={customername}
            margin="dense"
            id="name"
            label="Name"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={customerPhoneNum}
            margin="dense"
            id="Phone Number"
            label="Phone Number"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            onChange={customerMobileNum}
            autoFocus
            margin="dense"
            id="name"
            label="Mobile Number"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={customerEmail}
            margin="dense"
            id="name"
            label="Email"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            onChange={customerAddress}
            autoFocus
            margin="dense"
            id="name"
            label="Address"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseCreateCustomer} color="primary">
            Cancel
          </Button>
          <Button onClick={createCustomer} color="primary">
            Create Customer
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default CreateCustomerModal;
