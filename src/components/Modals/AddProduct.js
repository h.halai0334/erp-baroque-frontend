import React, { useState, useEffect } from "react";
// import "./userManagement/App.css";
import { forwardRef } from "react";
import Avatar from "react-avatar";
import Grid from "@material-ui/core/Grid";
import CreateProduct from "../Modals/CreateProduct";
import { Button } from "reactstrap";
//import Button from "@material-ui/core/Button";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();
const filter = createFilterOptions();

function AddProduct(props) {
  //   const [openProdcutForInquiry, setProdcutForInquiry] = React.useState(false);
  // const [openInquiry, setOpenInquiry] = React.useState(false);
  const [openProduct, setOpenProduct] = React.useState(false);
  const [openCreate, setopenCreate] = React.useState(false);
  //   const [eachproductQuantityData, setEachProductQuantity] = useState("");
  const [eachproductDescripData, setEachProductDescrip] = useState("");
  let [productModal, setProductModal] = useState({
    // productList: [],
    id: null,
    name: "",
    productQunatity: null,
    productDescription: "",
  });

  //   const [openProdcutForInquiry, setProdcutForInquiry] = React.useState(false);
  // const handleClose = () => {
  //   setOpenInquiry(false);
  //   //setOpenProduct(false);
  // };
  const eachProductQuantity = (event) => {
    SetProductQuantityModal(event.target.value);
  };
  const eachProductDescrip = (event) => {
    setProductDescriptionModal(event.target.value);
  };
  const handleCloseInquiryProduct = () => {
    props.setProdcutForInquiry(false);
    //setOpenProduct(false);
  };
  // api methods
  function onSelectEachProduct(e, value) {
    if (value) {
      if (value.id) {
        const productInfo = {
          id: value.id,
          title: value.title,
        };
        setProductInfoInProductModal(productInfo);
      } else {
        CreateProductModal();
      }
    }
  }

  function CreateProductModal() {
    setopenCreate(true);
  }
  function onSelectProdct(e, value) {
    props.getAddProduct(productModal);
    if (productModal.id) {
      toast("Product Has Been Added Successfully");
    } else {
      toast("kindly Add Atleast 1 product");
    }

    props.handleClose();
    // setOpenInquiry(false);
    // debugger;
    // const completeTableData = {
    //   items: [
    //     {
    //       productId: tableDataForEachRow[0].id,
    //       description: eachproductDescripData,
    //       quantity: +eachproductQuantityData,
    //     },
    //   ],
    // };
    // // tableData = [...tableData, completeTableData];
    // setTableData(tableData);
  }
  // set modal functions
  function setProductInfoInProductModal(productInfo) {
    productModal = {
      ...productModal,
      id: productInfo.id,
      name: productInfo.title,
    };
    setProductModal(productModal);
  }
  function SetProductQuantityModal(value) {
    productModal = {
      ...productModal,
      productQunatity: value,
    };
    setProductModal(productModal);
  }
  function setProductDescriptionModal(value) {
    productModal = {
      ...productModal,
      productDescription: value,
    };
    setProductModal(productModal);
  }
  //   if (productModal.productList.length === 0) {
  //     setProductlistToProductModal(props.productList);
  //   }
  function handleCloseCreateProduct() {
    setopenCreate(false);
  }

  return (
    <div className="App">
      <Dialog
        open={props.openProdcutForInquiry}
        onClose={props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Add Product</DialogTitle>
        <DialogContent>
          <Autocomplete
            // value={value}
            onChange={onSelectEachProduct}
            filterOptions={(options, params) => {
              const filtered = filter(options, params);

              // Suggest the creation of a new value
              if (params.inputValue !== "") {
                filtered.push({
                  inputValue: params.inputValue,
                  title: `Add "${params.inputValue}"`,
                });
              }

              return filtered;
            }}
            options={props.productList}
            getOptionLabel={(option) => option.title}
            // getOptionLabel={(option) => {
            //   // Value selected with enter, right from the input
            //   if (typeof option === "string") {
            //     return option;
            //   }
            //   // Add "xxx" option created dynamically
            //   if (option.inputValue) {
            //     return option.inputValue;
            //   }
            //   // Regular option
            //   return option.title;
            // }}
            renderOption={(option) => option.title}
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Select Product Name"
                variant="outlined"
              />
            )}
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={eachProductQuantity}
            margin="dense"
            id="Description"
            label="Quantity"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={eachProductDescrip}
            margin="dense"
            id="name"
            label="Description"
            type="email"
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={handleCloseInquiryProduct} color="primary">
            Cancel
          </Button>
          <Button onClick={onSelectProdct} color="primary">
            Add Product
          </Button>
        </DialogActions>
      </Dialog>
      {openCreate ? (
        <CreateProduct
          // productList={productList}
          openCreate={openCreate}
          handleCloseCreateProduct={handleCloseCreateProduct}
          // setProdcutForInquiry={setProdcutForInquiry}
          //getAddProduct={getAddProduct}
        />
      ) : null}
    </div>
  );
}

export default AddProduct;
