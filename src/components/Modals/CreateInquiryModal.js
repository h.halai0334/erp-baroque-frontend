import React, { useState, useEffect } from "react";
// import "./userManagement/App.css";
import { forwardRef } from "react";
import Avatar from "react-avatar";
import Grid from "@material-ui/core/Grid";
import CreateProduct from "../Modals/CreateProduct";
import { Button } from "reactstrap";
//import Button from "@material-ui/core/Button";
import CreateCustomerModal from "../Modals/CreateCustomer";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const filter = createFilterOptions();
toast.configure();

function CreateInquiryModal(props) {
  const [openCreate, setopenCreate] = React.useState(false);
  const [customerList, setCustomerList] = useState([]);
  const [inquiryTitle, setInquiryTitle] = useState("");
  const [inquiryDescription, setInquiryDescription] = useState("");
  const [customersDetails, setCustomerDetails] = useState({});
  let [createInquiryModel, setInquiryModel] = useState({
    customerInfo: {
      id: null,
      title: "",
    },
    description: "",
    title: "",
  });
  useEffect(() => {
    // getUsers(setData);
    getCustomerList(setCustomerList);
  }, []);

  function getCustomerList(setCustomerList) {
    const token = localStorage.getItem("login");
    // let baseURL =
    //   "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Product/dropDown?Page=1";
    // var searchParams = new URLSearchParams(query);

    // for (let [key, value] of Object.entries(query)) {
    //   console.log("key, value: ", key, value);
    //   searchParams.append(key, value);
    // }
    // debugger;
    //let URl = baseURL + searchParams.toString();

    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Customer/dropDown?Page=1",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        const customerList = res.data.map((d) => {
          return {
            id: d.id,
            title: d.name,
          };
        });
        setCustomerList(customerList);
      });
  }

  function onSelectCustomer(e, value) {
    if (value) {
      if (value.id) {
        const customerInfo = {
          id: value.id,
          title: value.title,
        };

        createInquiryModel = {
          ...createInquiryModel,
          customerInfo,
        };
        setInquiryModel(createInquiryModel);
      } else {
        CreateCustomerModalFromInquiryModal();
      }
    }
  }

  const handleChangeInquiryTitle = (event) => {
    createInquiryModel = {
      ...createInquiryModel,
      title: event.target.value,
    };
    setInquiryModel(createInquiryModel);
  };

  const handleChangeInquiryDescrioption = (event) => {
    createInquiryModel = {
      ...createInquiryModel,
      description: event.target.value,
    };
    setInquiryModel(createInquiryModel);
  };
  function createInquiryApi(payload) {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Inquiry",
      {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      }
    )
      .then((response) => response.json())
      .then((res) => {
        if (res.id) {
          toast("Inquiry Has Bee Created Successfully");
          props.handleCloseInquiryModal();
        } else {
          toast("Found Some Error");
        }
      });
  }
  function createInquiry() {
    console.log("createInquiryModel: ", createInquiryModel);
    const userId = localStorage.getItem("userId");
    const token = localStorage.getItem("login");

    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Inquiry/code",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        // code = res.code;
        const payload = {
          userId: userId,
          customerId: createInquiryModel.customerInfo.id,
          description: createInquiryModel.description,
          title: createInquiryModel.title,
          code: res.code,
          items: props.tableData.map((d) => ({
            productId: d.id,
            quantity: +d.productQunatity,
            description: d.productDescription,
          })),
        };

        createInquiryApi(payload);
      });
  }
  function CreateCustomerModalFromInquiryModal() {
    setopenCreate(true);
  }

  function handleCloseCreateCustomer() {
    setopenCreate(false);
  }

  return (
    <div className="App">
      <Dialog
        open={props.openInquiry}
        onClose={props.handleCloseInquiryModal}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create Inquiry</DialogTitle>
        <DialogContent>
          <Autocomplete
            // value={value}
            onChange={onSelectCustomer}
            filterOptions={(options, params) => {
              const filtered = filter(options, params);

              // Suggest the creation of a new value
              if (params.inputValue !== "") {
                filtered.push({
                  inputValue: params.inputValue,
                  title: `Add "${params.inputValue}"`,
                });
              }

              return filtered;
            }}
            options={customerList}
            getOptionLabel={(option) => option.title}
            renderOption={(option) => option.title}
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Select Customer Name"
                variant="outlined"
              />
            )}
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={handleChangeInquiryTitle}
            margin="dense"
            id="title"
            label="Title"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={handleChangeInquiryDescrioption}
            margin="dense"
            id="description"
            label="Description"
            type="text"
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={props.handleCloseInquiryModal} color="primary">
            Cancel
          </Button>
          <Button onClick={createInquiry} color="primary">
            Create Inquiry
          </Button>
        </DialogActions>
      </Dialog>
      {openCreate ? (
        <CreateCustomerModal
          openCreate={openCreate}
          handleCloseCreateCustomer={handleCloseCreateCustomer}
        />
      ) : null}
    </div>
  );
}

export default CreateInquiryModal;
