import React, { useState, useEffect } from "react";
// import "./userManagement/App.css";
import { forwardRef } from "react";
import Avatar from "react-avatar";
import Grid from "@material-ui/core/Grid";
import CreateProduct from "../Modals/CreateProduct";
import { Button } from "reactstrap";
//import Button from "@material-ui/core/Button";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();
const filter = createFilterOptions();

function CreateCostEstimationModal(props) {
  useEffect(() => {
    // getUsers(setData);
    getContractorList();
    getMaterialList();
    // getCustomerList(setCustomerList);
  }, []);

  const getContractorList = () => {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Contractor/dropDown?Page=1",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        setcontractorList(res.data);
      });
  };
  const getMaterialList = () => {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Material/dropDown?Page=1",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        setmaterialList(res.data);
      });
  };
  //   const [openProdcutForInquiry, setProdcutForInquiry] = React.useState(false);
  // const [openInquiry, setOpenInquiry] = React.useState(false);
  const [openProduct, setOpenProduct] = React.useState(false);
  const [openCreate, setopenCreate] = React.useState(false);
  const [contractorList, setcontractorList] = useState([]);
  const [materialList, setmaterialList] = useState([]);
  //   const [eachproductQuantityData, setEachProductQuantity] = useState("");
  const [eachproductDescripData, setEachProductDescrip] = useState("");
  let [productModal, setProductModal] = useState({
    // productList: [],
    id: null,
    name: "",
    productQunatity: null,
    productDescription: "",
  });

  let [costEstimationModal, setCostestimationModal] = useState({
    productId: null,
    productQuantity: null,
    processName: "",
    contractorId: null,
    expectedDays: null,
    expectedStartDate: Date.now(),
    expectedEndDate: Date.now(),
    expectedTotalCost: null,
    materialId: null,
    materialQuantity: null,
    materialCost: null,
    expectedLaborCost: null,
  });

  //   const [openProdcutForInquiry, setProdcutForInquiry] = React.useState(false);
  // const handleClose = () => {
  //   setOpenInquiry(false);
  //   //setOpenProduct(false);
  // };
  const eachProductQuantity = (event) => {
    SetProductQuantityModal(event.target.value);
  };
  //   const eachProductDescrip = (event) => {
  //     setProductDescriptionModal(event.target.value);
  //   };
  const closeCreateCostEstimationModal = () => {
    props.handlecloseModal(false);
    //setOpenProduct(false);
  };
  // api methods

  // if (value) {
  //   if (value.productId) {
  //     const productInfo = {
  //       id: value.productId,
  //       title: value.product,
  //     };
  //     setProductInfoInProductModal(productInfo);
  //   } else {
  //     // CreateProductModal();
  //   }
  // }

  //   function onSelectProdct(e, value) {
  //     props.getAddProduct(productModal);
  //     if (productModal.id) {
  //       toast("Product Has Been Added Successfully");
  //     } else {
  //       toast("kindly Add Atleast 1 product");
  //     }
  //   }
  function onSelectContractor(e, value) {
    costEstimationModal = {
      ...costEstimationModal,
      contractorId: value.id,
    };

    setCostestimationModal(costEstimationModal);
  }
  function onSelectMaterail(e, value) {
    costEstimationModal = {
      ...costEstimationModal,
      materialId: value.id,
    };
    setCostestimationModal(costEstimationModal);
  }
  // set modal functions
  function setProductInfoInProductModal(productInfo) {
    productModal = {
      ...productModal,
      id: productInfo.id,
      name: productInfo.title,
    };
    setProductModal(productModal);
  }
  function SetProductQuantityModal(value) {
    productModal = {
      ...productModal,
      productQunatity: value,
    };
    setProductModal(productModal);
  }
  //   function setProductDescriptionModal(value) {
  //     costEstimationModal = {
  //       ...costEstimationModal,
  //       productId: value.id,
  //     };

  //     setCostestimationModal(costEstimationModal);
  //   }
  //   if (productModal.productList.length === 0) {
  //     setProductlistToProductModal(props.productList);
  //   }
  //   function handleCloseCreateProduct() {
  //     setopenCreate(false);
  //   }
  function onSelectEachProduct(e, value) {
    costEstimationModal = {
      ...costEstimationModal,
      productId: value.productId,
    };

    setCostestimationModal(costEstimationModal);
  }
  const handleStartDate = (date) => {
    costEstimationModal = {
      ...costEstimationModal,
      expectedStartDate: date,
    };
    setCostestimationModal(costEstimationModal);
  };
  const handleEndDate = (date) => {
    costEstimationModal = {
      ...costEstimationModal,
      expectedEndDate: date,
    };
    setCostestimationModal(costEstimationModal);
  };

  const onChangeProductQuantity = (event) => {
    costEstimationModal = {
      ...costEstimationModal,
      productQuantity: +event.target.value,
    };
    setCostestimationModal(costEstimationModal);
  };
  const onChangeprocessName = (event) => {
    costEstimationModal = {
      ...costEstimationModal,
      processName: event.target.value,
    };
    setCostestimationModal(costEstimationModal);
  };
  const onchangeexpectedDays = (event) => {
    costEstimationModal = {
      ...costEstimationModal,
      expectedDays: +event.target.value,
    };
    setCostestimationModal(costEstimationModal);
  };
  const onChangeCost = (event) => {
    costEstimationModal = {
      ...costEstimationModal,
      expectedTotalCost: +event.target.value,
    };
    setCostestimationModal(costEstimationModal);
  };
  const onchangeMaterialQuantity = (event) => {
    costEstimationModal = {
      ...costEstimationModal,
      materialQuantity: +event.target.value,
    };
    setCostestimationModal(costEstimationModal);
  };
  const onChangeMaterialCost = (event) => {
    costEstimationModal = {
      ...costEstimationModal,
      materialCost: +event.target.value,
    };
    setCostestimationModal(costEstimationModal);
  };

  const onchangeLaborCost = (event) => {
    costEstimationModal = {
      ...costEstimationModal,
      expectedLaborCost: +event.target.value,
    };
    setCostestimationModal(costEstimationModal);
  };
  const onCreateCostEstimation = () => {
    console.log(costEstimationModal);
    const userId = localStorage.getItem("userId");
    const inquiryId = props.selectedRowData.id;
    debugger;

    const payload = {
      inquiryId: inquiryId,
      userId: userId,
      products: [
        {
          productId: costEstimationModal.productId,
          quantity: costEstimationModal.productQuantity,
          processes: [
            {
              name: costEstimationModal.processName,
              contractors: [
                {
                  contractorId: costEstimationModal.contractorId,
                  expectedDays: costEstimationModal.expectedDays,
                  expectedStartDate: costEstimationModal.expectedStartDate,
                  expectedEndDate: costEstimationModal.expectedEndDate,
                  expectedTotalCost: costEstimationModal.expectedTotalCost,
                  materials: [
                    {
                      materialId: costEstimationModal.materialId,
                      quantity: costEstimationModal.materialQuantity,
                      cost: costEstimationModal.materialCost,
                    },
                  ],
                  expectedLaborCost: costEstimationModal.expectedLaborCost,
                },
              ],
            },
          ],
        },
      ],
    };

    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/CostEstimation",
      {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      }
    )
      .then((response) => response.json())
      .then((res) => {
        if(res.data ){
          toast("Cost Estimation Added Successfully")
        }
        else{
          toast("Found Some Error")
        }
        // if (res.id) {
        //   toast("Product Has Bee Created Successfully");
        //   props.handleCloseCreateProduct();
        // } else {
        //   toast("Found Some Error");
        // }
      });
  };

  return (
    <div className="App">
      <Dialog
        open={props.openCreateCostEstimationModal}
        onClose={props.handlecloseModal}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create Cost Estimation</DialogTitle>
        <DialogContent>
          <DialogContent>
            <TextField
              //   autoFocus
              onChange={onChangeprocessName}
              margin="dense"
              id="ProcessName"
              label="Process Name"
              type="text"
              fullWidth
            />
          </DialogContent>
          <DialogContent>
            <Autocomplete
              // value={value}
              onChange={onSelectEachProduct}
              filterOptions={(options, params) => {
                const filtered = filter(options, params);

                // Suggest the creation of a new value
                if (params.inputValue !== "") {
                  filtered.push({
                    inputValue: params.inputValue,
                    title: `Add "${params.inputValue}"`,
                  });
                }

                return filtered;
              }}
              options={props.selectedRowData.products}
              getOptionLabel={(option) => option.product}
              // getOptionLabel={(option) => {
              //   // Value selected with enter, right from the input
              //   if (typeof option === "string") {
              //     return option;
              //   }
              //   // Add "xxx" option created dynamically
              //   if (option.inputValue) {
              //     return option.inputValue;
              //   }
              //   // Regular option
              //   return option.title;
              // }}
              renderOption={(option) => option.product}
              style={{ width: 300 }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Select Product Name"
                  variant="outlined"
                />
              )}
            />
          </DialogContent>
          <DialogContent>
            <TextField
              //   autoFocus
              onChange={onChangeProductQuantity}
              margin="dense"
              id="ProductQuatity"
              label="Product Quatity"
              type="number"
              fullWidth
            />
          </DialogContent>

          <DialogContent>
            <Autocomplete
              // value={value}
              onChange={onSelectContractor}
              filterOptions={(options, params) => {
                const filtered = filter(options, params);

                // Suggest the creation of a new value
                if (params.inputValue !== "") {
                  filtered.push({
                    inputValue: params.inputValue,
                    title: `Add "${params.inputValue}"`,
                  });
                }

                return filtered;
              }}
              options={contractorList}
              getOptionLabel={(option) => option.name}
              // getOptionLabel={(option) => {
              //   // Value selected with enter, right from the input
              //   if (typeof option === "string") {
              //     return option;
              //   }
              //   // Add "xxx" option created dynamically
              //   if (option.inputValue) {
              //     return option.inputValue;
              //   }
              //   // Regular option
              //   return option.title;
              // }}
              renderOption={(option) => option.name}
              style={{ width: 300 }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Select Contractor"
                  variant="outlined"
                />
              )}
            />
          </DialogContent>
          <DialogContent>
            <TextField
              //   autoFocus
              onChange={onchangeexpectedDays}
              margin="dense"
              id="Days"
              label="Expected Days"
              type="text"
              fullWidth
            />
          </DialogContent>
          <DialogContent>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Grid container justify="space-around">
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="MM/dd/yyyy"
                  margin="normal"
                  id="Expected Start Date"
                  label="Expected Start Date"
                  value={costEstimationModal.expectedStartDate}
                  onChange={handleStartDate}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                />
              </Grid>
            </MuiPickersUtilsProvider>
          </DialogContent>
          <DialogContent>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Grid container justify="space-around">
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="MM/dd/yyyy"
                  margin="normal"
                  id="Expected End Date"
                  label="Expected End Date"
                  value={costEstimationModal.expectedEndDate}
                  onChange={handleEndDate}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                />
              </Grid>
            </MuiPickersUtilsProvider>
          </DialogContent>

          <DialogContent>
            <Autocomplete
              // value={value}
              onChange={onSelectMaterail}
              filterOptions={(options, params) => {
                const filtered = filter(options, params);

                // Suggest the creation of a new value
                if (params.inputValue !== "") {
                  filtered.push({
                    inputValue: params.inputValue,
                    title: `Add "${params.inputValue}"`,
                  });
                }

                return filtered;
              }}
              options={materialList}
              getOptionLabel={(option) => option.name}
              // getOptionLabel={(option) => {
              //   // Value selected with enter, right from the input
              //   if (typeof option === "string") {
              //     return option;
              //   }
              //   // Add "xxx" option created dynamically
              //   if (option.inputValue) {
              //     return option.inputValue;
              //   }
              //   // Regular option
              //   return option.title;
              // }}
              renderOption={(option) => option.name}
              style={{ width: 300 }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Select Matrial"
                  variant="outlined"
                />
              )}
            />
          </DialogContent>
          <DialogContent>
            <TextField
              //   autoFocus
              onChange={onchangeMaterialQuantity}
              margin="dense"
              id="MaterialQuantity"
              label="Materail Quantity"
              type="number"
              fullWidth
            />
          </DialogContent>
          <DialogContent>
            <TextField
              //   autoFocus
              onChange={onChangeMaterialCost}
              margin="dense"
              id="MaterialCost"
              label="Materail Cost"
              type="number"
              fullWidth
            />
          </DialogContent>
          <DialogContent>
            <TextField
              //   autoFocus
              onChange={onchangeLaborCost}
              margin="dense"
              id="LaborCost"
              label="Labor Cost"
              type="number"
              fullWidth
            />
          </DialogContent>
          <DialogContent>
            <TextField
              //   autoFocus
              onChange={onChangeCost}
              margin="dense"
              id="cost"
              label="Total Cost"
              type="number"
              fullWidth
            />
          </DialogContent>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeCreateCostEstimationModal} color="primary">
            Cancel
          </Button>
          <Button onClick={onCreateCostEstimation} color="primary">
            Create Cost Estimation
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default CreateCostEstimationModal;
