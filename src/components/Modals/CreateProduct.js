import React, { useState, useEffect } from "react";
// import "./userManagement/App.css";
import { forwardRef } from "react";
import Avatar from "react-avatar";
import Grid from "@material-ui/core/Grid";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  FormGroup,
  Form,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";
//import Button from "@material-ui/core/Button";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
var code = "";
toast.configure();
function CreateProduct(props) {
  const [productNameData, setProductName] = useState("");
  const [openProductModal, setOpenProductModal] = useState(false);
  const [productDescripData, setProductDescrip] = useState("");
  const [productSaleData, setProductSale] = useState("");

  const productname = (event) => {
    setProductName(event.target.value);
  };
  const productDescrip = (event) => {
    setProductDescrip(event.target.value);
  };
  const productSale = (event) => {
    setProductSale(event.target.value);
  };

  function handleCloseProductModal() {
    setOpenProductModal(true);
  }
  useEffect(() => {
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Product/code",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((res) => {
        code = res.code;
      });
  }, []);
  const createProductItem = () => {
    let errorList = [];

    // if (productNameData === undefined) {
    //   errorList.push("Please enter first name");
    // }
    // if (productDescripData === undefined) {
    //   errorList.push("Please enter a valid Description");
    // }
    // if (productSaleData === undefined) {
    //   errorList.push("Please enter a valid Sale");
    // }

    if (errorList.length < 1) {
      console.log(code);
      //no error
      const token = localStorage.getItem("login");
      fetch(
        "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Product",
        {
          method: "POST",
          headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            code: code,
            name: productNameData,
            description: productDescripData,
            image: "",
            sale: +productSaleData,
          }),
        }
      )
        .then((response) => response.json())
        .then((res) => {
          if (res.id) {
            toast("Product Has Bee Created Successfully");
            props.handleCloseCreateProduct();
          } else {
            toast("Found Some Error");
          }
        });

      //   api

      //     .get("/users")
      //     .then((res) => {
      //       console.log("res", res);
      //       setData(res.data.data);
      //     })
      //     .catch((error) => {
      //       console.log("Error");
      //     });
      //
    }
  };
  function handleCloseCreateProductModal() {
    props.handleCloseCreateProduct();
  }
  return (
    <div className="App">
      <Dialog
        open={props.openCreate}
        onClose={props.handleCloseCreateProduct}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create Product</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            onChange={productname}
            margin="dense"
            id="name"
            label="Name"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={productDescrip}
            margin="dense"
            id="Description"
            label="Description"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogContent>
          <TextField
            autoFocus
            onChange={productSale}
            margin="dense"
            id="name"
            label="Sale"
            type="email"
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={handleCloseCreateProductModal} color="primary">
            Cancel
          </Button>
          <Button onClick={createProductItem} color="primary">
            Create Product
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default CreateProduct;
