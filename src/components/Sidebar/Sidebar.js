/*!

=========================================================
* Paper Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

* Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { NavLink } from "react-router-dom";
import { Nav } from "reactstrap";
import { Link } from "react-router-dom";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
import { ProSidebar, Menu, MenuItem, SubMenu } from "react-pro-sidebar";
import logo from "logo.svg";
import "react-pro-sidebar/dist/css/styles.css";
var ps;

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
    this.sidebar = React.createRef();
    this.state = {
      role: "",
    };
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.sidebar.current, {
        suppressScrollX: true,
        suppressScrollY: false,
      });
    }
    const token = localStorage.getItem("login");
    fetch(
      "http://ec2-52-41-153-125.us-west-2.compute.amazonaws.com/api/v1/Role",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          role: data.data[0],
        });
      });
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }
  render() {
    let userManagement;
    if (this.state.role) {
      userManagement = (
        <ProSidebar>
          <Menu iconShape="square">
            <SubMenu title="User Management">
              <MenuItem linkButton={true} href="/icons">
                USER Add/Update/Delete
                <Link to="/admin/addUser" />
              </MenuItem>
              <MenuItem linkButton={true} href="/icons">
                Customer Add/Update/Delete
                <Link to="/admin/addCustomer" />
              </MenuItem>
            </SubMenu>
          </Menu>
          <Menu iconShape="square">
            <SubMenu title="INVENTORY">
              <MenuItem linkButton={true} href="/icons">
                Material Add/Update/Delete
                <Link to="/admin/material" />
              </MenuItem>
              <MenuItem linkButton={true} href="/icons">
                Product Add/Update/Delete
                <Link to="/admin/product" />
              </MenuItem>
            </SubMenu>
          </Menu>
          <Menu iconShape="square">
            <SubMenu title="CONTRACTOR">
              <MenuItem linkButton={true} href="/icons">
                Contractor Add/Update/Delete
                <Link to="/admin/contractor" />
              </MenuItem>
            </SubMenu>
          </Menu>
        </ProSidebar>
      );
    }
    return (
      <div
        className="sidebar"
        data-color={this.props.bgColor}
        data-active-color={this.props.activeColor}
      >
        <div className="logo">
          <a
            href="https://www.creative-tim.com"
            className="simple-text logo-mini"
          >
            <div className="logo-img">
              <img src={logo} alt="react-logo" />
            </div>
          </a>
          <a
            href="https://www.creative-tim.com"
            className="simple-text logo-normal"
          >
            Creative Tim
          </a>
        </div>
        <div className="sidebar-wrapper" ref={this.sidebar}>
          <Nav>
            {userManagement}
            {this.props.routes.map((prop, key) => {
              if (key > 5) {
                return (
                  <li
                    className={
                      this.activeRoute(prop.path) +
                      (prop.pro ? " active-pro" : "")
                    }
                    key={key}
                  >
                    <NavLink
                      to={prop.layout + prop.path}
                      className="nav-link"
                      activeClassName="active"
                    >
                      <i className={prop.icon} />

                      <p>{prop.name}</p>
                    </NavLink>
                  </li>
                );
              }
            })}
          </Nav>
        </div>
      </div>
    );
  }
}

export default Sidebar;
